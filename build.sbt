// Project Name
name := "pension-analyser"

// Version
version := "1.0"

// Scala Version used
scalaVersion := "2.12.2"

// Scala Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % Test

// Akka Actor framework
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.2",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.2" % Test
)

// CSV Writer
libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.4"

// Enable native packaging
enablePlugins(JavaAppPackaging)
