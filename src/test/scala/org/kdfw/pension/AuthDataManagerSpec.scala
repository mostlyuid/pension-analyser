package org.kdfw.pension

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration.Duration

/**
  * Test cases for Authentication Data Manager
  */
class AuthDataManagerSpec extends TestKit(ActorSystem("AuthDataManagerSpec"))
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  /**
    * Shutdown Actor system after Test run is complete
    */
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  /**
    * The Authentication Data Manager must send back it's results
    */
  "The Authentication data manager " must {

    "respond with number of authentication failures present in the stream" in {

      // Parse CSV File
      val authDataManager = system.actorOf(AuthDataManager.props("./output.csv"))
      val source = getClass.getResource("/auth_log_kdfw.csv").getFile
      source should not be Nil
      authDataManager ! source

      // Expect result
      val result = receiveOne(Duration(5, TimeUnit.SECONDS))
      result match {

        // The _ and asInstanceOf is to avoid Type erasure issues with JVM. If I use the actual type,
        // I will be rewarded with a warning of type erasures.
        case stats: AuthStats =>
          stats.failures should be >= 0
          stats.requests should be >= 0
          stats.failures should be <= stats.requests

        case _ => true should be(false)
      }

      // Wait till Auth Data Manager terminates
      val probe = TestProbe("Probe")
      probe watch authDataManager
      probe.expectTerminated(authDataManager, Duration(5, TimeUnit.MINUTES))
    }

  }

}
