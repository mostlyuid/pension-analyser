package org.kdfw.pension

import java.time.LocalDate
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.kdfw.csv.AuthResponse
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration.Duration

/**
  * Test Case for Pensioners
  */
class PensionerSpec extends TestKit(ActorSystem("AuthDataManagerSpec"))
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  /**
    * Shutdown Actor system after Test run is complete
    */
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  /**
    * Check if bucketization works correctly for 2 buckets
    */
  "The Pensioner actor " must {
    "respond with only 2 buckets for the given data" in {

      // Create pensioner
      val pensioner = system.actorOf(Pensioner.props(ID("A", 1)))

      // Send Two authentication attempts in two different months
      pensioner ! Attempt(LocalDate.of(2012, 4, 5), AuthResponse(value = false, code = 300))
      pensioner ! Attempt(LocalDate.of(2012, 5, 5), AuthResponse(value = false, code = 300))
      pensioner ! Pensioner.REPORT_TRANSACTIONS

      // Get a Single message back as a result
      val result = receiveOne(Duration(5, TimeUnit.SECONDS))
      result match {

        /**
          * Match Map value
          */
        case result: (_, _) =>
          result._1.asInstanceOf[ID].name should be("A")
          result._1.asInstanceOf[ID].pensionId should be(1)
          val map = result._2.asInstanceOf[Map[Bucket, Transactions]]
          map.size should be(2)
          map.values.foreach(txn => {
            txn.daysVisited should be(1)
            txn.success should be(0)
            txn.failure should be(1)
          })

        /**
          * Match any other message type
          */
        case _ => assertTypeError("Unknown Type as result")
      }
    }

    /**
      * Check if bucketization works correctly for 1 bucket
      */
    "respond with only 1 bucket for the given data" in {

      // Create pensioner
      val pensioner = system.actorOf(Pensioner.props(ID("B", 2)))

      // Send Two authentication attempts in two different months
      pensioner ! Attempt(LocalDate.of(2012, 4, 5), AuthResponse(value = false, code = 300))
      pensioner ! Attempt(LocalDate.of(2012, 4, 6), AuthResponse(value = false, code = 300))
      pensioner ! Attempt(LocalDate.of(2012, 4, 7), AuthResponse(value = false, code = 300))
      pensioner ! Attempt(LocalDate.of(2012, 4, 7), AuthResponse(value = true, code = 0))
      pensioner ! Pensioner.REPORT_TRANSACTIONS

      // Get a Single message back as a result
      val result = receiveOne(Duration(5, TimeUnit.SECONDS))
      result match {

        /**
          * Match Map value
          */
        case result: (_, _) =>
          result._1.asInstanceOf[ID].name should be("B")
          result._1.asInstanceOf[ID].pensionId should be(2)
          val map = result._2.asInstanceOf[Map[Bucket, Transactions]]
          map.size should be(1)
          map.values.foreach(txn => {
            txn.daysVisited should be(3)
            txn.success should be(1)
            txn.failure should be(3)
          })

        /**
          * Match any other message type
          */
        case _ => assertTypeError("Unknown Type as result")
      }
    }


  }
}
