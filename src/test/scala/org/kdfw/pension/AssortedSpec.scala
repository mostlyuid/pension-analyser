package org.kdfw.pension

import org.scalatest.{FlatSpec, Matchers}

/**
  * Other Tests which do not fit into a pattern
  */
class AssortedSpec extends FlatSpec with Matchers {

  /**
    * String Tests for Bucket
    */
  it should "Convert Bucket into String" in {

    val month1 = Bucket(4, 2016)
    val month2 = Bucket(6, 2015)
    month1.toString should be ("April 2016")
    month2.toString should be ("June 2015")
  }
}
