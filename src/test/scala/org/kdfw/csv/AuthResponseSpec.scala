package org.kdfw.csv

import org.scalatest._

/**
  * Test Cases for AuthResponse
  */
class AuthResponseSpec extends FlatSpec with Matchers {

  /**
    * Check if success strings are parsed correctly
    */
   it should "Parse success strings properly" in {
     val input    = " Success "
     val response = AuthResponse(input)
     response.code should  be (0)
     response.value should be (true)
  }

  /**
    * Checks if failure strings are parsed correctly
    */
  it should "Parse failure strings properly" in {
    val input    = " Failed "
    val response = AuthResponse(input)
    response.code should  be (0)
    response.value should be (false)
  }

  /**
    * Checks if failure strings with Error codes are parsed correctly
    */
  it should "Parse failure strings with code properly" in {
    val input    = " Failed (Biometric mismatch (Code : 300)) "
    val response = AuthResponse(input)
    response.code should  be (300)
    response.value should be (false)
  }
}
