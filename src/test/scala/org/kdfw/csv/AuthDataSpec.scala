package org.kdfw.csv

import org.scalatest.{FlatSpec, Matchers}

/**
  * Testing for AuthData
  */
class AuthDataSpec extends FlatSpec with Matchers {

  /**
    * Should Parse the Full CSV Line properly
    */
  it should "Parse the happy path correctly" in {

    val input = "1,4/5/2016,D,B,S,NAME,100,B3580799,DFB00024,Failed (300),,,,,,,,,,"
    val data  = AuthData(input)
    data.name           should be ("NAME")
    data.pensionId      should be (100)
    data.response.value should be (false)
    data.response.code  should be (300)
    data.date.getDayOfMonth should be (4)
    data.date.getMonthValue should be (5)
    data.date.getYear       should be (2016)
  }

  /**
    * It should pass on Whacko inputs
    */
  it should "pass on whacko inputs" in {

    val input = "1,4/5/2016,D,B,S,NAME,100,05ba&000b&0102{CDB66A46-614F-4C00-80A9-21446BADCF56},X,Failed,,,,,,,,,,"
    val data  = AuthData(input)
    data.name           should be ("NAME")
    data.pensionId      should be (100)
    data.response.value should be (false)
    data.response.code  should be (0)
    data.date.getDayOfMonth should be (4)
    data.date.getMonthValue should be (5)
    data.date.getYear       should be (2016)
  }

  /**
    * Fail on Wrong dates
    */
  it should "Fail incorrect dates" in {

    val input = "1,4/15/2016,D,B,S,NAME,100,B3580799,DFB00024,Failed (300),,,,,,,,,,"
    try {
      AuthData(input)
      false should be (true)
    } catch {
      case _: Exception => true
    }
  }

  /**
    * Fail on incomplete data
    */
  it should "Fail on incomplete data" in {

    val input = "IYPE,,,,,,,,,702410019,,,,,,,,,,"
    try {
      AuthData(input)
      false should be (true)
    } catch {
      case _: Exception => true
    }
  }
}
