package org.kdfw.csv

import org.scalatest.{FlatSpec, Matchers}

/**
  * Test Class for CSV Reader
  */
class ReaderSpec extends FlatSpec with Matchers {

  /**
    * Test if the CSV Reader works correctly.
    */
  it should "Parse the CSV File correctly" in {

    val source   = getClass.getResource("/auth_log_kdfw.csv").getFile
    val reader   = new Reader(source)
    var authData: Option[AuthData] = None
    do {
      authData = reader.get()
    } while(authData.isDefined)

    // Checks
    val tuple2  = reader.stats()
    val errors  = tuple2._1
    val records = tuple2._2
    (errors + records) should be (52229)
  }

}
