package org.kdfw.pension


import akka.actor.{ActorSystem, Props}

/**
  * The CLI Wrapper. There are only two arguments. The raw Input file and the output File
  */
object CLI {

  /**
    * Main Function to Run
    * @param args Argument List
    */
  def main(args: Array[String]): Unit = {

    if (args.length < 2) {
      println("Need two arguments at least. The input CSV File and the output file")
      System.exit(1)
    }

    // Get Arguments
    val sourceFile = args(0)
    val destFile   = args(1)

    // Start the Actor System
    val actorSystem     = ActorSystem()
    val authDataManager = actorSystem.actorOf(Props(new AuthDataManager(destFile)))

    // Start the Auth Data Processing
    authDataManager ! sourceFile

    // Wait for 30 Seconds
    Thread.sleep(30000)

    // And shutdown the actorsystem
    actorSystem.terminate()
  }
}
