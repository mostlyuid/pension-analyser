package org.kdfw.pension

import java.text.SimpleDateFormat
import java.time.LocalDate

import akka.actor.{Actor, Props}
import org.kdfw.csv.AuthResponse

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * A Pensioner has a Name and Pension ID
  */
case class ID(name: String, pensionId: Int)

/**
  * Every pensioner attempts to get his/her pension.
  * @param date       Date of Attempt
  * @param response   Authentication Response
  */
case class Attempt(date:LocalDate, response:AuthResponse)

/**
  * A Bucket is basically a way to group all transactions made within a month
  * @param month Month
  * @param year  Year
  */
case class Bucket(month: Int, year: Int) {

  /**
    * Overriden version of toString to convert 4 => April, 5 => May and so on
    * @return String version of Bucket, For CSV Printing.
    */
  override def toString: String =  {
    val monthParse   = new SimpleDateFormat("MM")
    val monthDisplay = new SimpleDateFormat("MMMM")
    monthDisplay.format(monthParse.parse(month.toString)) + " " + year.toString
  }
}

/**
  * There are two ways  in which a denial happens. Outright denial (Never successfull) and multiple trips in
  * different days of the same month (increased transaction cost)
  * @param daysVisited      No. of days visited for authentication during that month
  * @param success          No. of times during the month in which bio. auth is successful
  * @param failure          No. of times during the month in which bio. auth failed
  */
case class Transactions(daysVisited: Int, success: Int, failure: Int)

/**
  * Based on authentication records, every Pensioner(Actor) produces a summary report for every month.
  * @param id      ID of the Pensioner
  * @param txns    Transactions
  */
case class Summary(id: ID, txns: Map[Bucket, Transactions])

/**
  * The Pensioner class
  */
class Pensioner(val id: ID) extends Actor  {

  /**
    * Handy Type declaration
    */
  type Attempts = mutable.MutableList[Attempt]

  /**
    * List of Attempts
    */
  val authAttempts = new Attempts()

  /**
    * Message Processor
    */
  def receive: PartialFunction[Any, Unit] = {

    /**
      * Save Attempts for later analysis
      */
    case attempt:Attempt => authAttempts += attempt

    /**
      * Analyse and Report Transactions
      */
    case Pensioner.REPORT_TRANSACTIONS =>
      val sentBy = sender()
      Future {
        val txns = bucketize
        sentBy ! Summary(id, txns)
      }
  }

  /**
    * Given the Internal Attempts, analyze them and report them into [Bucket, Transactions] format
    * @return Bucket Attempts into months
    */
  private def bucketize: Map[Bucket,Transactions]  = {
    authAttempts.groupBy(x => Bucket(x.date.getMonth.getValue, x.date.getYear))
                .mapValues(authAttempts => {
                  val days      = daysVisited(authAttempts)
                  val success   = successAttempts(authAttempts)
                  val failed    = failedAttempts(authAttempts)
                  Transactions(days, success, failed)
                })
  }

  /**
    * Given a list of Attempts, we here count the number of successes via a Fold Left Operation
    * @param attempts Attempts
    * @return No. of successes.
    */
  private def successAttempts(attempts: Attempts): Int = attempts.foldLeft(0)((v,a) => v + asInt(a.response.value))

  /**
    * Given a list of Attempts, we here count the number of failures via a Fold Left Operation
    * @param attempts Attempts
    * @return No. of failures.
    */
  private def failedAttempts(attempts: Attempts): Int = attempts.foldLeft(0)((v,a) => v + asInt(!a.response.value))

  /**
    *  Given a list of attempts, count the no. of unique days.
    * @param attempts Attempts
    * @return Unique Visits in the month
    */
  private def daysVisited(attempts: Attempts): Int = attempts.groupBy(_.date.getDayOfMonth).size

  /**
    * Convert Boolean to Integer
    * @param b Boolean
    * @return 1 of true, 0 if false
    */
  private def asInt(b:Boolean):Int = if (b) 1 else 0
}

/**
  * Pensioner Companion Object
  */
object Pensioner {

  /**
    * Constant for Reporting transactions
    */
  val REPORT_TRANSACTIONS = 0x1

  /**
    * Handy one line to create Pensioner Actor
    * @param id ID
    * @return Props
    */
  def props(id: ID): Props = Props(new Pensioner(id))
}
