package org.kdfw.pension

import akka.actor.{Actor, PoisonPill, Props}
import com.github.tototoshi.csv.CSVWriter

/**
  * Companion Object for Totalizer
  */
object Totalizer {

  /**
    * Handy one line to create Totalizer Actor
    * @param csvFile Output File name
    * @return Props
    */
  def props(csvFile: String): Props = Props(new Totalizer(csvFile))
}

/**
  * Totalizer is responsible for outputting the final result in the format we had wanted.
  */
class Totalizer(val csvFile: String) extends Actor {

  /**
    * Authentication Statistics
    */
  var authStats: Option[AuthStats] = None

  /**
    * Total number of pensioners
    */
  var totalPensioners: Int = 0

  /**
    * Processed records of Pensioners
    */
  var pensionersLeft: Int = 0

  /**
    * Total number of pensioners who had to visit multiple days
    */
  var multipleVists: Int = 0

  /**
    * Total number of pensioners who never got through at least once a month
    */
  var neverGotThrough: Int = 0

  /**
    * CSV File and the Header are written immediately
    */
  val writer: CSVWriter = createCSVFile()

  /**
    * Message loop
    * @return Partial Function
    */
  def receive: PartialFunction[Any, Unit] = {

    /**
      * A Simple Integer is the type used to send the total pensioners count
      */
    case count: Int =>
      totalPensioners     = count
      pensionersLeft      = count

    /**
      * Process Authentication Statistics
      */
    case value: AuthStats => authStats = Some(value)

    /**
      * Every Pensioner is sending us a Summary. Write it out to the CSV File and Compute running statistics
      */
    case summary: Summary =>
      summary.txns.foreach( kv => {
        writeBucket(summary.id, kv)
        if (kv._2.daysVisited > 1) {
          multipleVists += 1
        }
        if (kv._2.success == 0) {
          neverGotThrough += 1
        }
      })
      pensionersLeft -= 1
      if (pensionersLeft <= 0) {
        printStats()
        writer.close()
        self ! PoisonPill
        context.parent ! AuthDataManager.DONE
      }
  }

  /**
    * Create CSV File
    * @return CSVWriter Object
    */
  private def createCSVFile(): CSVWriter = {
    val writer = CSVWriter.open(csvFile)
    writer.writeRow(List("Name", "Pension ID", "Month", "Days Visited", "Success Count", "Failure Count"))
    writer
  }

  /**
    * Write the Bucket in CSV File
    * @param id   ID
    * @param item Bucket Item
    */
  private def writeBucket(id: ID, item:(Bucket, Transactions)): Unit = {
    writer.writeRow(Seq(
      id.name, id.pensionId.toString, item._1.toString,
      item._2.daysVisited.toString,
      item._2.success, item._2.failure
    ))
  }

  /**
    * Write out statistics on the Console
    */
  private def printStats(): Unit  = {

    println (s"Total Number of Authentication Failures: ${authStats.get.failures}")
    println (s"Total Number of Authentication Requests: ${authStats.get.requests}")
    println (s"Total Number of Unique Pensioners:       $totalPensioners")
    println (s"Pensioners who never got through:        $neverGotThrough")
    println (s"Pensioners who visited multiple days:    $multipleVists")

  }

}
