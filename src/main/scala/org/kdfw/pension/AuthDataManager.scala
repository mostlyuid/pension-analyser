package org.kdfw.pension

import akka.actor.{Actor, ActorRef, Props, PoisonPill}
import org.kdfw.csv.{AuthData, Reader}

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Authentication Statistics
  * @param failures Failures
  * @param requests Requests
  */
case class AuthStats(failures: Int, requests: Int)

/**
  * Companion object for Authentication Data Manager
  */
object  AuthDataManager {

  /**
    * Termination Message
    */
  val DONE = 0x1

  /**
    * Handy one line to create AuthDataManager Actor
    * @param csvFile Output File name
    * @return Props
    */
  def props(csvFile: String): Props = Props(new AuthDataManager(csvFile))
}

/**
  * Authentication Data Manager is responsible for creating Pensioner Actor and sending the AuthData message
  * to every one of them. It is the parent actor and is responsible for managing pensioners.
  */
class AuthDataManager(csvFile: String) extends Actor {

  /**
    * Internal map of Pensioner ID to their Actor Reference
    */
  private val pensioners = new mutable.HashMap[Int,ActorRef]()

  /**
    * Totalizing Actor
    */
  private val totalizer = context.actorOf(Totalizer.props(csvFile), name = "Totalizer")

  /**
    * Authentication Requests in the stream
    */
  private var authRequests = 0

  /**
    * Authentication Failures in the stream
    */
  private var authFailures = 0

  /**
    *  Message Processor for the Actor
    */
  def receive: PartialFunction[Any, Unit] = {

    /**
      * Reads the CSV File from the reader and emits AuthData to the right Pensioners. if the Pensioner does not
      * exist, it is created and the actor reference is added. The sender gets the count of authentication failures
      * and requests when done. The sentBy closure is required or else we will get a dead letter box issue. In general
      * closures are required when we are using a variable across context switches (Threads)
      */
    case csvFile:String =>
      val sentBy = sender()
      Future {
        readCSVFile(csvFile)
        val result = AuthStats(authFailures, authRequests)
        sentBy    ! result
        totalizer ! pensioners.size
        totalizer ! result
        pensioners.values.foreach(pensioner => pensioner ! Pensioner.REPORT_TRANSACTIONS)
    }

    /**
      * We have basically got response from some pensioner which reports it's transactions. Send it to the
      * totalizer. This is not a scalable design for a very large system, but good enough for the given
      * use case.
      */
    case summary:Summary => totalizer ! summary

    /**
      * When the totalizer reports that it is done, Send Poison Pills to all the Pensioner Actors and to self
      */
    case AuthDataManager.DONE =>
      pensioners.values.foreach(actor => actor ! PoisonPill)
      self ! PoisonPill
  }

  /**
    * Read the CSV File, Create actors and send the AuthData messages
    * @param csvFile Name of the CSV File
    */
  private def readCSVFile(csvFile: String): Unit = {

    // Read the CSV File
    val csvReader                 = new Reader(csvFile)
    var authData:Option[AuthData] = None

    // Read Authentication Data records. If needed create a new Pensioner(Actor) or use an existing actor.
    // And then send the authentication attempt message to every pensioner.
    do {
      authData = csvReader.get()
      if (authData.isDefined) {
        val data  = authData.get
        updateStats(data)
        val actor = pensionerReference(data)
        actor ! Attempt(data.date, data.response)
      }
    } while(authData.isDefined)
  }

  /**
    * Update Internal Statistics
    * @param authData Authentication Data
    */
  private def updateStats(authData: AuthData): Unit = {

    authRequests += 1
    if (!authData.response.value) {
      authFailures += 1
    }
  }

  /**
    * Given an authentication data, give back an Actor Reference
    * @param authData Authentication Data
    * @return Actor Reference
    */
  private def pensionerReference(authData: AuthData): ActorRef = {

    // Get the actor given a pensioner ID
    val actor = pensioners.get(authData.pensionId)

    // If there is no actor, then create a new Actor and add it to the store.
    if (actor.isEmpty) {
      val id    = ID(authData.name, authData.pensionId)
      val newActor = context.actorOf(Pensioner.props(id), name = s"Pensioner-${authData.pensionId}")
      pensioners.put(authData.pensionId, newActor)
      newActor
    } else {
      actor.get
    }

  }
}
