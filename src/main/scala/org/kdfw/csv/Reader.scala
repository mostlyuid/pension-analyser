package org.kdfw.csv

import scala.io.Source

/**
  * CSV Reader that emits records of AuthData during every iteration.
  *
  * @param name Name of the file.
  */
class Reader(name: String) {

  /**
    * Stream iterator
    */
  private val iterator: Iterator[String] = Source.fromFile(name).getLines()

  /**
    * No. of Authentication records which is nothing but number of lines in the file
    */
  private var records: Int = 0

  /**
    * No. of Error records which are skipped.
    */
  private var errors: Int = 0

  /**
    * Statistics on the Reader
    * @return A tuple of (error records, total records)
    */
  def stats(): (Int, Int) = Tuple2(errors, records)

  /**
    * Iterate through the stream and convert it into AuthData
    * @return
    */
  def get(): Option[AuthData] = {

    // Nothing to provide if iterator does not have the next item.
    if (!iterator.hasNext) {
      return None
    }

    // Since lines cannot be transformed into Auth Data, we have to do an iteration.
    while (iterator.hasNext) {

      // Get the next line
      val line = iterator.next()
      try {

        // Can it be transformed into an AuthData? And if so, return it
        val authData = AuthData(line)
        records += 1
        return Some(authData)

        // else go to the next record, but track errors.
      } catch {
        case _: Exception => errors += 1
      }
    }

    // OK We have reached the end of line.
    None
  }
}
