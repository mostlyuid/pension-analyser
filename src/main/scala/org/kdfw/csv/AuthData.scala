package org.kdfw.csv

import java.time.LocalDate

/**
  * Authentication Data as found in a single row
  * @param date Date on which authentication attempt is made
  * @param name Name of the person
  * @param pensionId Pension ID
  * @param response  Authentication Response
  */
case class AuthData(date: LocalDate, name: String, pensionId: Int, response:AuthResponse) {
}

/**
  * Companion object for AuthData
  */
object AuthData {

  /**
    * Create AuthData from a String input (Line input in a CSV File). The format is
    * LOG_ID, Date, DISTRICT_ID, BLOCK_ID, SOCIETY_ID, NAME, KDFWFB_ID, DEVICE_ID, UDC_ID, Code
    * (0)      (1)     (2)         (3)        (4)       (5)     (6)        (7)       (8)     (9)
    * @param input Line in a CSV File
    * @return AuthData
    */
  def apply(input: String): AuthData = {

    // Split and trim into array
    val array = input.split(",").map(_.trim)

    // Fill up all the required fields
    val date      = toDate(array(1))
    val name      = array(5)
    val pensionId = array(6).toInt
    val response  = AuthResponse(array(9))
    AuthData(date, name, pensionId, response)
  }

  /**
    * Convert the input in the form dd/mm/yy into string
    * @param input Input
    * @return Date
    */
  def toDate(input: String): LocalDate = {

    val array = input.split("/").map(_.toInt)
    LocalDate.of(array(2), array(1), array(0))
  }
}
