package org.kdfw.csv

import scala.util.matching.Regex

/**
  * An authentication response contains both the code and the message
 *
  * @param value   Did it succeed or failed?
  * @param code    Authentication Code
  */
case class AuthResponse(value: Boolean, code: Int) {
}

/**
  * Companion object for Auth Response
  */
object AuthResponse {

  /**
    * Regex Pattern for extracting out Code if it is present
    */
  val codePattern:  Regex = ".*([1-9][0-9]+).*".r

  /**
    * Extract out the value, code and Message from a String representation
    * @param input String value
    * @return AuthResponse
    */
  def apply(input: String): AuthResponse = {

    // Check for success string via ignore case comparison (?i)
    var response = false
    if (input.toLowerCase().contains("success")) {
      response = true
    }

    // Check of (Code:number) to get the code
    input match {
      case codePattern(code) => AuthResponse(response, code.toInt)
      case _                 => AuthResponse(response, 0)
    }
  }
}
