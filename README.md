Our aim is to analyze authentication logs that are available at 
http://www.kdfwf.org/index.php/device-access-log and answer the following questions: 

1. Total number of pensioners
2. Total number of persons who were denied pensions at least once in a month
3. Total number of persons who had to make multiple trips (on different days)to get their pensions. 
4. Total number of authentication requests
5. Total number of authentication failures

We will also be providing a output.csv file, which will show the following summarized view for every month for
every pensioner: 
Name, Pension ID, Month, Days Visited, Success Count, Failure Count

The way to read the CSV File data is 
Pensioner visited 2 days on April and faced biometric failure 2 times and a success one time. Other details such as
location, society etc. can be obtained from the raw log file. 

## Log files
The log files are located at data/ directory. There is only source Log file there and it is scrapped directly from
the web site, using a script. The raw file does not contain any Personally sensitive information except what is 
already shown in the web site.
 
The processed output log file is available at $ROOT/output.csv. It's format is specified above. 

#Approach
We will be using Scala Actors to do the above. We have a raw CSV file which has the following columns:
Id, Date, District,	Block, Society, Name, Pension Id, Device Id, UDC Id, Response Code

This will be further trimmed to since these are sufficient to derive the answers to the above questions. 
Date, Name, Pension Id, Response Code

Scala Actor framework is used (I am bored) to compute the above values. The top level program is a simple command line
program which uses the actor framework. The code is extensively documented so that it is easy for others to understand.
However a previous working knowledge of Scala is required for comprehending it.

# Build
To build the project have a look at .gitlab-ci.yml file in the repository. If you wish to reproduce the results using
the program, I wrote tag my twitter handle @iam_anandv


